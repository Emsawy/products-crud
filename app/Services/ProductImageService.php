<?php

namespace App\Services;

use App\Exceptions\ProductImageException;
use App\Models\ProductImage;
use Exception;
use Illuminate\Support\Facades\Storage;

class ProductImageService {

    /**
     * Move file to the product image path
     * @var File $image
     * @return string
     */
    public function moveImage($image)
    {
        try {
            $imageName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME) . time() . '.'. pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
            return basename($image->move(config('file.product_images_dir'), $imageName));
        } catch(Exception $e) {
            throw new ProductImageException('Faild Moving Image');
        }
    }

    /**
     * Create product image record
     * @var integer $productId
     * @var String $imageName
     * @return ProductImage
     */
    public function createProductImge($productId, $imageName)
    {
        try {
            return ProductImage::create([
                'product_id' => $productId,
                'path' => $imageName
            ]);
        } catch(Exception $e) {
            throw new ProductImageException('Faild Creating Image');
        }
    }

}