<?php

namespace App\Listerners;

use App\Events\NewProductAddedEvent;
use App\Mail\SendNewProductAddedEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendNewProductMailListerner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewProductAddedEvent  $event
     * @return void
     */
    public function handle(NewProductAddedEvent $event)
    {
        Mail::to($event->data['mail'])->send(new SendNewProductAddedEmail($event->data));
    }
}
