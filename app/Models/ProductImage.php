<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'path'
    ];

    protected $appends = ['src'];

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'featured_image');
    }

    /**
     * Get user profile picture
     *
     * returns string (full image path)
     */
    public function getSrcAttribute()
    {
        return url(config('file.product_images_path') . $this->path);
    }

}
