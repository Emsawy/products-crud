<?php

namespace App\Http\Controllers;

use App\Events\NewProductAddedEvent;
use App\Exceptions\ProductImageException;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Services\ProductImageService;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    /**
     * @var ProductImageService
     */
    private $productImageService;

    public function __construct(ProductImageService $productImageService) 
    {
        $this->productImageService = $productImageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $products = Product::with(['images', 'featuredImage'])->get();
        } catch(Exception $e) {
            return redirect()->back()->with(['error' => 'feching data faild']); 
        }
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(productRequest $request)
    {
        try {
            $product = Product::create([
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
            ]);
    
            $this->updateProductImages($request, $product);
            
        } catch(ProductImageException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        };

        $product->featuredImage;
        $product->images;

        $data = [
            'mail' => 'owner@product.com',
            'name' => $product->name,
            'price' => $product->price,
            'description' => $product->description,
            'mailFrom' => config('mail.from.address'),
            'mailName' => config('mail.from.name'),
            'mailTitle' => 'New Product Added',
        ];

        event(new NewProductAddedEvent($data));

        return response()->json([
            'message' => 'Successfully add product',
            'product' => $product
        ], 200);
    }
    /**
     * Update product images
     * @param Request $request
     * @param Product $product
     */
    public function updateProductImages($request, $product)
    {
        // create featured image
        $imageName = $this->productImageService->moveImage($request->featuredImage);
        $productImage = $this->productImageService->createProductImge($product->id, $imageName);
        $product->featured_image = $productImage->id;
        $product->save();

        // create product images
        if($request->has('productImages')) {
            foreach($request->productImages as $productImage) {
                $imageName = $this->productImageService->moveImage($productImage);
                $this->productImageService->createProductImge($product->id, $imageName);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(productRequest $request, Product $product)
    {
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
        ]);

        $this->removeProductImages($product);

        $this->updateProductImages($request, $product);

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->removeProductImages($product);
        $product->delete();
        Session::flash('success', 'Successfully delete');
        return redirect()->route('products.index');
    }

    public function removeProductImages($product)
    {
        $productImages = $product->images;
        foreach($productImages as $productImage) {
            File::delete(config('file.product_images_dir') . $productImage->path);
        }
    }
}
