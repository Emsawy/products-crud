<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:30'],
            'price' => ['required', 'gt:0'],
            'description' => ['required', 'min:10', 'max:199'],
            'featuredImage' => ['required', 'image', 'max:2048', 'mimes:jpeg,png,jpg,gif,svg'],
            'productionImages.*' => ['image', 'max:2048', 'mimes:jpeg,png,jpg,gif,svg'],
            'productionImages' => ['array', 'between:1,30',], // this max is just for avoiding resource wasting by hackers
        ];
    }
}
