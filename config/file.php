<?php

return [
    'product_images_dir' => storage_path('app/public/products/'),
    'product_images_path' => 'storage/products/'
];