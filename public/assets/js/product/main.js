// configuration modals
var modals = {
    addProduct: {
        id: 'addModal',
        el: $('#addModal'),
        validator: null
    }
}

// configuration validation rules
var validationRules = {
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 30
        },
        price: {
            // TODO the rest of rules existed in backend
            required: true,
        },
        description: {
            // TODO the rest of rules existed in backend
            required: true,
        },
        featured_image: {
            // TODO the rest of rules existed in backend
            required: true
        }
    }
}

modals.addProduct.validator = $('#addProductForm').validate(validationRules);

// start add user form
$('#addProductForm').submit(function (event) {

    event.preventDefault();
    let form = $(event.target);
    if (!form.valid()) {
        return;
    }
    
    var formData = new FormData(this);

    $.ajax({
        url: addProductRoute,
        method: 'POST',
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
    })
        .then((res) => {
            toastr.success(res.message);
            let itemsHTML = populateProduct(res.product);
            $('#product-container').append(itemsHTML);
            closeModals(modals.addProduct);
        })
        .catch((err) => {
            setErrors(modals.addProduct.validator, err.responseJSON.errors);
        })
});

// close modal when submitting
function closeModals(modal) {
    // reset form fields
    resetModal(modal.el)
    
    if(modal.validator != null)
        modal.validator.resetForm();

}

function resetModal(modal) {

    // reset fields
    modal.find('input[name="name"]').val('')
    modal.find('input[name="price"]').val('')
    modal.find('input[name="description"]').val('')
    modal.find('input[name="featuredImage"]').val('')
    modal.find('input[name="productImages[]"]').val('')

    // hide modal and modal background
    hideModal(modal)
}

function hideModal(modal) {
    modal.modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

$('.close-add-form').on('click', function(){
    closeModals(modals.addProduct);
})

// merge backend errors with frontend errors
function setErrors(validator, errors) {
    Object.keys(errors).map((error) => { errors[error] = errors[error][0] });
    validator.showErrors(errors);
}

function populateProduct(product) {

    let card = $("#mock-card").clone().html()
    
    card = card.replace(/\+NAME\+/g, product.name)
    card = card.replace(/\+DESCRIPTION\+/g, product.description)
    card = card.replace(/\+FEATURED_IMAGE\+/g, product.featured_image.src)
    card = card.replace(/\++PRODUCT_ID+\+/g, product.id)

    let imagesHtml = '';
    for($i = 0; $i < product.images.length; $i++) {
        imagesHtml += `<div class="d-inline-flex">
            <img src="${product.images[$i].src}" alt="" style="width: 100px">
        </div>`
    }

    card = card.replace(/\++PRODUCT_IMAGES+\+/g, imagesHtml)

    return card;
}
