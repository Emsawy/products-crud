<div class="" id="mock-card" style="display: none">
  <div class="grid col-4 mb-5">
    <div class="grid__item">
        <div class="card"><img class="card__img" src="+FEATURED_IMAGE+" alt="Desert">
            <div class="card__content">
                <h1 class="card__header">+NAME+</h1>
                <p class="card__text" style="min-height: 150px">
                    +DESCRIPTION+
                </p>
                <div class="mb-3">
                    <h1>All Product Images</h1>
                    +PRODUCT_IMAGES+
                </div>
                <div class="d-flex" style="justify-content: space-between">
                    <a href="{{route('products.edit', '+PRODUCT_ID+')}}" style="text-decoration: none">
                        <button class="card__btn">
                            EDIT<span>&rarr;</span>
                        </button>
                    </a>  
                    <form action="{{route('products.destroy', '+PRODUCT_ID+')}}" method="POST">
                      @csrf
                      @method('delete')
                      <button type="submit" class="card__btn">DELETE 
                          <span>&rarr;</span>
                      </button>
                  </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>