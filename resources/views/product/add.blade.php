<!-- Modal -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Add Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="addProductForm" class="row mx-2" action="{{route('products.store')}}" method="POST" style="width: 470px" enctype="multipart/form-data">
            @csrf
            <div class="col-12 pt-4">
              <h4 class="pb-3 primary-color font-400">Product Name</h4>
              <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Product Name" name="name" value="{{old('name') ?? ''}}">
                @error('name')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12 pt-4">
              <h4 class="pb-3 primary-color font-400">Price</h4>
              <input type="number" step="0.01" class="form-control @error('price') is-invalid @enderror" placeholder="Price" value="{{old('price') ?? ''}}" name="price">
                @error('price')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12 pt-4">
              <h4 class="pb-3 primary-color font-400">Description</h4>
              <textarea class="form-control @error('description') is-invalid @enderror" placeholder="Product Name" name="description">{{old('description') ?? ''}}</textarea>
                @error('description')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12 pt-4">
                <h4 class="pb-3 primary-color font-400">Featured Image</h4>
                <input type="file" class="form-control @error('featuredImage') is-invalid @enderror" name="featuredImage">
                @error('featuredImage')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-12 pt-4">
                <h4 class="pb-3 primary-color font-400">Product Images</h4>
                <input type="file" class="form-control" name="productImages[]" multiple>
            </div>
            <div class="col-12 pt-4">
                <button type="submit" class="btn btn-success">Add</button>
            </div>

        </form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close-add-form">Close</button>
      </div>
    </div>

  </div>
</div>