## Steps:
## git clone https://Emsawy@bitbucket.org/Emsawy/products-crud.git
## composer install
## create database and change env file database name and password
## php artisan migrate
## php artisan key:generate
## php artisan storage:link
## set mailtrap configuration to test adding product
## php artisan serve
## visit http://localhost:8000/products